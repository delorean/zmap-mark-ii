# zmap - mark II

zmap tuned for stealth. evades fingerprinting by threat intelligence firms, service providers, and everyone in between.

# Why?

the original zmap contains trackers in its headers that are used to identify the traffic as belonging to zmap.

this makes it easy for threat intelligence agencies, isp's, and anyone else to fingerprint zmap traffic and generate alerts/tags/abuse reports accordingly.

# Installation

**dependencies**: cmake, gmp, gengetopt, libpcap, flex, byacc, json-c, libunistring, pkg-config, libjudy

to install the dependencies on debian based systems:

```
sudo apt install -y build-essential cmake libgmp3-dev gengetopt libpcap-dev flex byacc libjson-c-dev pkg-config libunistring-dev libjudy-dev
```

compile it with

```
git clone https://git.supernets.org/delorean/zmap-mark-ii
cd zmap-mark-ii
cmake .
make -j4
```

then install it via

 
```
sudo make install
```

# Usage

```
zmap -h
```

refer to [zmap's usage](https://github.com/zmap/zmap/wiki/Getting-Started-Guide)